<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet 
version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
>
<!-- ========================== Imports =========================== -->
<xsl:import href="transform.xsl"/>

<!-- ========================== Output =========================== -->
<xsl:output 
method="xml" 
indent="yes"
encoding="utf-8"/>

<!-- ========================== Templates =========================== -->
<xsl:template name="svg.title">
<title>Raccourcis Gnome Shell</title>
<desc>A wallpaper for power user, version 1.0</desc>
</xsl:template>


<xsl:template name="svg.defs.graphic">
<g id="graphic">
<rect x="0" y="0" width="1300" height="950" rx="15" fill="none" stroke="#{$graphic.color}" stroke-width="2"/>
<text x="650" y="65" text-anchor="middle" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="30px" font-weight="normal">Raccourcis clavier Gnome</text>

<!-- colonne 1 -->

<!-- section -->
<text x="50" y="150" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Fenêtre</text>
<line x1="50" y1="155" x2="625" y2="155" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>
<!-- 50 px -->
<text x="50" y="200" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2190;</text>
<text x="270" y="200" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Placer la fenêtre à gauche</text>

<text x="50" y="230" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2192;</text>
<text x="270" y="230" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Placer la fenêtre à droite</text>

<text x="50" y="260" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2191;</text>
<text x="270" y="260" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Maximiser la fenêtre</text>

<text x="50" y="290" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2193;</text>
<text x="270" y="290" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Relacher la fenêtre</text>

<text x="50" y="320" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + H</text>
<text x="270" y="320" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Minimiser la fenêtre</text>

<text x="50" y="350" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">alt + F4</text>
<text x="270" y="350" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Fermer la fenêtre</text>

<!-- 70 px entre 2 sections successives -->
<!-- section -->
<text x="50" y="420" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Capture d'écran</text>
<line x1="50" y1="425" x2="625" y2="425" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>
<!-- 50 px -->
<text x="50" y="470" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">PrtSc</text>
<text x="270" y="470" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture d'écran</text>

<text x="50" y="500" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">alt + PrtSc</text>
<text x="270" y="500" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture fenêtre courante</text>

<text x="50" y="530" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x21D1; + PrtSc</text>
<text x="270" y="530" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture d'une zone d'écran</text>

<text x="50" y="560" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + PrtSc</text>
<text x="270" y="560" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture écran vers presse-papier</text>

<text x="50" y="590" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + alt + PrtSc</text>
<text x="270" y="590" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture fenêtre vers presse-papier</text>

<text x="50" y="620" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + &#x21D1; + PrtSc</text>
<text x="270" y="620" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Capture zone vers presse-papier</text>

<text x="50" y="650" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + alt + &#x21D1; + R</text>
<text x="270" y="650" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Enregistrer l'écran</text>

<!-- 70 px entre 2 sections successives -->
<!-- section -->

<text x="50" y="720" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Système</text>
<line x1="50" y1="725" x2="625" y2="725" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>

<text x="50" y="770" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + L</text>
<text x="270" y="770" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Verrouiller la session</text>

<text x="50" y="800" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + alt + Suppr</text>
<text x="270" y="800" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Quitter la session</text>

<!-- colonne 2 -->

<!-- section -->
<text x="675" y="150" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Bureaux virtuels</text>
<line x1="675" y1="155" x2="1250" y2="155" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>

<text x="675" y="200" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2912;</text>
<text x="895" y="200" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Bureau précédent</text>

<text x="675" y="230" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2913;</text>
<text x="895" y="230" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Bureau suivant</text>

<text x="675" y="260" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + alt + &#x2912;</text>
<text x="895" y="260" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Déplacer la fenêtre vers l'espace -1</text>

<text x="675" y="290" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + alt + &#x2913;</text>
<text x="895" y="290" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Déplacer la fenêtre vers l'espace +1</text>

<text x="675" y="320" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x21D1; + Super + &#x231C;&#x2196;</text>
<text x="895" y="320" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Déplacer la fenêtre vers l'espace 1</text>

<text x="675" y="350" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x21D1; + Super + &#x2198;&#x231F;</text>
<text x="895" y="350" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Déplacer la fenêtre vers l'espace n</text>

<text x="675" y="380" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x231C;&#x2196;</text>
<text x="895" y="380" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Aller à l'espace 1</text>

<text x="675" y="410" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + &#x2198;&#x231F;</text>
<text x="895" y="410" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Aller à l'espace n</text>


<!-- section -->
<text x="675" y="480" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Actions</text>
<line x1="675" y1="485" x2="1250" y2="485" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>

<text x="675" y="530" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + alt + T</text>
<text x="895" y="530" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Lancer un terminal</text>

<text x="675" y="560" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + E</text>
<text x="895" y="560" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Ouvrir le dossier personnel</text>

<text x="675" y="590" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + M/V</text>
<text x="895" y="590" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Ouvrir les notifications</text>


<!-- section -->
<text x="675" y="660" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="23px" font-weight="normal">Applications</text>
<line x1="675" y1="665" x2="1250" y2="665" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>

<text x="675" y="710" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super + A</text>
<text x="895" y="710" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Ouvrir les applications</text>

<text x="675" y="740" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">Super</text>
<text x="895" y="740" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Voir les toutes les fenêtres</text>

<text x="675" y="770" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + &#x21E5;</text>
<text x="895" y="770" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Changer d'application</text>

<text x="675" y="800" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">ctrl + alt+ &#x21E5;</text>
<text x="895" y="800" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="normal">Coverflow</text>

<!-- 70 px entre 2 sections successives -->
<!-- légende -->
<line x1="50" y1="870" x2="1250" y2="870" stroke="#{$graphic.color}" fill="#{$graphic.color}" stroke-width="2"/>
<text x="200" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x21E5; : Tab</text>
<text x="350" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x21D1; : Maj</text>
<text x="500" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x231C;&#x2196; : Début</text>
<text x="650" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x2198;&#x231F; : Fin</text>
<text x="800" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x2912; : PgUp</text>
<text x="950" y="900" text-anchor="start" fill="#{$graphic.color}" stroke="#{$graphic.color}" font-family="sans-serif" font-size="18px" font-weight="bold">&#x2913; : PgDn</text>
</g>
</xsl:template>


<xsl:template match="/">
	
<svg
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
version="1.1"
width="{$display.width}px"
height="{$display.height}px"
viewbox="0 0 {$display.width} {$display.height}">

<xsl:call-template name="svg.title"/>
<defs>
<xsl:call-template name="svg.defs.background"/>
<xsl:call-template name="svg.defs.mask"/>
<xsl:call-template name="svg.defs.graphic"/>
</defs>

<xsl:call-template name="svg.draw"/>

</svg>
	
</xsl:template>	
<!-- ========================== Default actions =========================== -->
<xsl:template match="text()"/>
</xsl:stylesheet>
