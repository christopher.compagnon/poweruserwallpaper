<?xml version="1.0" encoding="utf-8"?>
<!-- 
 ========================== Infos ===========================
 Description : Génération de PowerUserWallpaper
 Auteur : C. Compagnon
 Courriel : admin@firenode.net
 Site : http://www.firenode.net
 ============================================================
 
 Version			date				Description
 ............................................................
 1.0				2020-06-26		Version initiale
 ............................................................
 
 Suppléments :

 ============================================================
 -->
<xsl:stylesheet 
version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
> 

<!-- ========================== Imports =========================== -->

<!-- ========================== Paramètres =========================== -->

<!-- ========================== Variables =========================== -->
<xsl:variable name="display.width" select="data/display/width"/> <!-- définition de la largeur de l'affichage de sortie -->
<xsl:variable name="display.height" select="data/display/height"/> <!-- définition de la hauteur de l'affichage de sortie -->
<xsl:variable name="graphic.width" select="1300"/> <!-- définition de la largeur de la zone en surimpression -->
<xsl:variable name="graphic.height" select="950"/> <!-- définition de la hauteur de la zone en surimpression -->

<xsl:variable name="image.center.x" select="$display.width div 2"/>
<xsl:variable name="image.center.y" select="$display.height div 2"/>
<xsl:variable name="image.data" select="data/image/base64"/>
<xsl:variable name="mime.type" select="data/image/@type"/>
<xsl:variable name="graphic.color" select="data/color"/>
<xsl:variable name="mask.opacity" select="data/opacity"/>
<xsl:variable name="mask.width" select="$graphic.width + 100"/> <!-- marge de 50 px -->
<xsl:variable name="mask.height" select="$graphic.height + 100"/> <!-- marge de 50 px -->
<xsl:variable name="background.color" select="data/display/background"/>
<!-- ========================== Règles =========================== -->
<!-- xsltproc ne gère pas bien les booléens
<xsl:variable name="image.enabled" as="xs:boolean">
<xsl:choose>
<xsl:when test="count(data/image) = 1"><xsl:value-of select="true()"/></xsl:when>
<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>
-->
<xsl:variable name="image.enabled">
<xsl:choose>
<xsl:when test="count(data/image) = 1">true</xsl:when>
<xsl:otherwise>false</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<!-- xsltproc ne gère pas bien les booléens
<xsl:variable name="background.enabled" as="xs:boolean">
<xsl:choose>
<xsl:when test="count(data/display/background) = 1"><xsl:value-of select="true()"/></xsl:when>
<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>
-->

<xsl:variable name="background.enabled">
<xsl:choose>
<xsl:when test="count(data/display/background) = 1">true</xsl:when>
<xsl:otherwise>false</xsl:otherwise>
</xsl:choose>
</xsl:variable>


<!--
le graphique fait 1300 x 950 , le centre est alors en (650,475)
l'image 2560 x 1440, le centre est alors en (1280,720)
il faut décaler le graphique de (1280,720) - (650,475) = (630,245)
-->

<!-- 
Pour un écran de 1366 x 768: le graphique est trop petit, il faut le grandir
scale = 1.2
pour un écran de 2560 x 1440 : le graphique est trop grand, il faut le réduire
scale = 0.8
-->

<xsl:variable name="graphic.scale">
<xsl:choose>
<xsl:when test="$display.height &lt; 850">0.8</xsl:when>
<xsl:when test="1000 &gt;= $display.height and $display.height &lt; 1200">0.9</xsl:when>
<xsl:when test="$display.height &gt;= 1200">0.8</xsl:when>
<xsl:otherwise>1.0</xsl:otherwise>
</xsl:choose>	
</xsl:variable>

<!-- <xsl:variable name="graphic.scale" select="0.8"/> -->
<xsl:variable name="graphic.scale.x" select="($graphic.width * $graphic.scale) div 2"/>
<xsl:variable name="graphic.scale.y" select="($graphic.height * $graphic.scale) div 2"/>
<xsl:variable name="graphic.translate.x" select="$image.center.x - $graphic.scale.x"/>
<xsl:variable name="graphic.translate.y" select="$image.center.y - $graphic.scale.y"/>

<xsl:variable name="mask.scale.x" select="($mask.width * $graphic.scale) div 2"/>
<xsl:variable name="mask.scale.y" select="($mask.height * $graphic.scale) div 2"/>
<xsl:variable name="mask.translate.x" select="$image.center.x - $mask.scale.x"/>
<xsl:variable name="mask.translate.y" select="$image.center.y - $mask.scale.y"/>
<!-- ========================== Templates =========================== -->

<xsl:template name="svg.draw">
<use xlink:href="#background"/>
<use xlink:href="#mask" transform="translate({$mask.translate.x},{$mask.translate.y}) scale({$graphic.scale})"/>
<use xlink:href="#graphic" transform="translate({$graphic.translate.x},{$graphic.translate.y}) scale({$graphic.scale})"/>
</xsl:template>


<xsl:template name="svg.defs.background">
<xsl:choose>

<!-- cas de l'image définie -->
<xsl:when test="$image.enabled = 'true'">
<xsl:message>Incoporating the image…</xsl:message>
<image id="background" x="0" y="0" width="{$display.width}" height="{$display.height}" xlink:href="data:{$mime.type};base64,{$image.data}"/>
</xsl:when>

<!-- cas de l'image non définie -->
<xsl:when test="$background.enabled = 'true'">
<xsl:message>No image : set the background with a color</xsl:message>
<rect id="background" x="0" y="0" width="{$display.width}" height="{$display.height}" fill="#{$background.color}" stroke="#{$background.color}" stroke-width="1"/>
</xsl:when>

</xsl:choose>
</xsl:template>


<xsl:template name="svg.defs.mask">
<!-- définition du masque -->	
<rect id="mask" x="0" y="0" width="{$mask.width}" height="{$mask.height}" rx="15"  fill="#ffffff" fill-opacity="{$mask.opacity}"/>
</xsl:template>

</xsl:stylesheet>
