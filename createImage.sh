#!/bin/sh
#set -x
#------------------------------------------------------------------
# Description
#------------------------------------------------------------------
#@(#) PowerUseWallpaper : Création de l'image SVG en ligne de commande
#@(#)------------------------------------------------------------------
#@(#) Création d'un papier-peint pour les Power Users, incluant les raccourcis clavier en surimpression.
#@(#) Version : 1.0
#@(#) Auteur : C. Compagnon
#@(#) Licence : CC0
#------------------------------------------------------------------
# Fonctions
#------------------------------------------------------------------
print_error () {
# affiche une erreur sur la sortie erreur
echo "$@" >&2
}

list () {
# liste les modèles disponibles
echo "Modèles disponibles :"
cat ./templates/templates.lst
exit 0
}

usage () {
echo "Usage: ${0} [-f nom de fichier] [-b couleur] [-d nombrexnombre] [-o décimal] [-c couleur] [-w nom de fichier] [-t modèle] [-l] [-i] [-h]"
echo "-f (filename) : chemin + nom vers le fichier qui servira d'image de fond."
echo "-b (background) : couleur (hexadécimale) du fond si pas d'utilisation d'image."
echo "-d (display) : dimension de l'image finale, normalement la taille de l'écran affichant l'image produite. Si non définie, alors la même valeur que l'option -s."
echo "-o (opacité) : opacité du masque utilisé pour augmenter le constraste. Valeur décimale, comprise entre 0.00 et 1.00. 0.00 si pas définie."
echo "-c (color) : couleur (hexadécimale) du tracé de la surimpression. ffffff (blanc) si pas définie."
echo "-w (wallpaper) : chemin + nom vers le fichier SVG généré. Si pas défini, même chemin que la source, avec pour nom wallpaper.svg par défaut."
echo "-t (template) : modèle de génération SVG."
echo "-l (list) : liste des modèles de génération disponibles."
echo "-i (install) : installe l'image générée comme papier-peint."
echo "-h (help) : affichage de l'aide."
}

is_number () {
# teste si le paramètre est un nombre
FILTER="$(echo ${1} | sed 's/[[:digit:]]//g')"
if [ "x${FILTER}x" = "xx" ]
then
echo 0
else
echo 1
fi
}
#------------------------------------------------------------------
# Variables
#------------------------------------------------------------------

#------------------------------------------------------------------
# Programme
#------------------------------------------------------------------
# Management of parameters
while getopts ":f:d:c:o:b:w:t:lh" PARAM
do
case "${PARAM}" in

"f")
# Fichier image à utiliser un fond
IMG_FILE="${OPTARG}"
HAS_IMAGE="true"
;;

"d")
# display : dimension de l'écran
DISPLAY_SIZE=${OPTARG}
DISPLAY_WIDTH=$(echo ${DISPLAY_SIZE} | cut -d'x' -f1)
DISPLAY_HEIGHT=$(echo ${DISPLAY_SIZE} | cut -d'x' -f2)
HAS_DISPLAY="true"

# DISPLAY_WIDTH est un nombre
IS_NUMBER=$(is_number ${DISPLAY_WIDTH})
[ ${IS_NUMBER:-1} -ne 0 ]  && print_error "${DISPLAY_WIDTH}  n'est pas un nombre entier" && exit 1

# DISPLAY_HEIGHT est un nombre
IS_NUMBER=$(is_number  ${DISPLAY_HEIGHT})
[ ${IS_NUMBER:-1} -ne 0 ]  && print_error "${DISPLAY_HEIGHT}  n'est pas un nombre entier" && exit 1
;;

"c")
# color : couleur du tracé
COLOR_DISPLAY=${OPTARG}
;;

"o")
# opacity : opacité du masque
DISPLAY_OPACITY=${OPTARG}
HAS_OPACITY="true"
;;

"b")
# background : le fond n'est pas une image, mais une couleur unie  (en hexadécimal)
BACKGROUND_COLOR=${OPTARG}
HAS_BACKGROUND="true"
;;

"w")
# wallpaper : Fichier image à génerer
OUTPUT_IMG="${OPTARG}"
HAS_TARGET="true"
;;

"t")
# template : modèle de génération à utiliser
TEMPLATE="./templates/${OPTARG}.xsl"
HAS_TEMPLATE="true"
;;

"l")
# Liste des modèles disponibles
list
;;

"h")
# Aide
usage
exit 0
;;

"i")
# install : défifint l'image générée comme papier-peint
HAS_INSTALL="true"
;;

?)
# Usage
usage
exit 1
;;

esac

done
#------------------------------------------------------------------
# Controls of options
# Si le fichier défini et backgraound défini, alors l'image gagne
[ "${HAS_IMAGE:-false}" = "true" -a "${HAS_BACKGROUND:-false}" = "true" ] && HAS_BACKGROUND="false" 

# Si le fichier source est défini mais qu'il nexiste pas, alors erreur
[ "${IMG_FILE:-none}" != "none" -a  ! -f "${IMG_FILE}" ] && print_error "Le fichier ${IMG_FILE} est introuvable à l'endroit indiqué." && exit 1

# Si le fichier n'est pas défini, il faut d'option background
[ "${HAS_IMAGE:-false}" != "true" -a "${HAS_BACKGROUND:-false}" != "true" ] && print_error "Il faut définir un fichier source (-f) ou une couleur de fond (-b)." && exit 1

# Si pas de source et pas de cible, alors erreur
[ "${HAS_IMAGE:-false}" != "true" -a "${HAS_TARGET:-false}" != "true" ] && print_error "Vous devez définir une source (-f) ou une cible (-w)." && exit 1

# Si l'opacité n'est pas définie, alors elle est invisible (0.00)
[ "${HAS_OPACITY:-false}" != "true" ] && DISPLAY_OPACITY="0.00"

# Il faut définir la dimension d'affichage
[ "${HAS_DISPLAY:-false}" != "true" ] && print_error "La dimension d'affichage (-d) est obligatoire." && exit 1

# Il faut définir le modèle
[ "${HAS_TEMPLATE:-false}" != "true" ] && print_error "Le modèle n'est pas défini --> gnome.shell.shortcuts.fr"
[ "${HAS_TEMPLATE:-false}" != "true" ] && TEMPLATE="gnome.shell.shortcuts.fr"

TEMPLATE_FILE="./templates/${TEMPLATE:-none}.xsl"
[ ! -f ${TEMPLATE_FILE} ] && print_error "Le modèle ${TEMPLATE:-none} est introuvable" && exit 1

# L'opacité est-elle un décimal ?
IS_DECIMAL="$(echo ${DISPLAY_OPACITY} | cut -c1-4 | grep "[0-1].[0-9][0-9]" | wc -l)"
[ ${IS_DECIMAL:-0} -ne 1 ]  && print_error "${DISPLAY_OPACITY}  n'est pas un nombre décimal au format N.NN." && exit 1

if [ "${HAS_IMAGE:-false}" = "true" ] 
then
DIR_NAME="$(dirname ${IMG_FILE})"
DIR_TMP="${DIR_NAME}"
FILE_NAME="$(basename ${IMG_FILE})"
FILE_EXT="$(echo ${FILE_NAME} | awk -F'.' '{print $NF}' | tr [:upper:] [:lower:])"
#------------------------------------------------------------------
# Conversion du fichier en base64
DATA_FILE="${DIR_TMP}/data.$$.base64"
cat "${IMG_FILE}" | openssl enc -base64 | tr -d "\n" > ${DATA_FILE}

case "${FILE_EXT}" in

"jpeg")
MIME_TYPE="image/jpeg"
;;

"jpg")
MIME_TYPE="image/jpeg"
;;

"png")
MIME_TYPE="image/png"
;;

esac

fi

[ "${HAS_TARGET:-false}" = "true" ] && DIR_TMP="$(dirname ${OUTPUT_IMG})"
#------------------------------------------------------------------
# Création du fichier XML de données
DATA_XML="${DIR_TMP}/data.$$.xml"
echo '<?xml version="1.0" encoding="utf-8"?>' > ${DATA_XML}
echo '<data>' >> ${DATA_XML}
echo "<color>${COLOR_DISPLAY:-ffffff}</color>" >> ${DATA_XML}
echo "<opacity>${DISPLAY_OPACITY:-0.00}</opacity>" >> ${DATA_XML}
echo '<display>' >> ${DATA_XML}
echo "<width>${DISPLAY_WIDTH}</width>" >> ${DATA_XML}
echo "<height>${DISPLAY_HEIGHT}</height>" >> ${DATA_XML}
[ "${HAS_BACKGROUND:-false}" = "true" ] && echo "<background>${BACKGROUND_COLOR}</background>" >> ${DATA_XML}
echo '</display>' >> ${DATA_XML}
[ "${HAS_IMAGE:-false}" = "true" ] && {
echo "<image type=\"${MIME_TYPE}\">" >> ${DATA_XML}
#echo "<width>${IMG_WIDTH}</width>" >> ${DATA_XML}
#echo "<height>${IMG_HEIGHT}</height>" >> ${DATA_XML}
echo "<base64>$(cat ${DATA_FILE})</base64>" >> ${DATA_XML}
echo '</image>' >> ${DATA_XML}
}
echo '</data>' >> ${DATA_XML}

#------------------------------------------------------------------
# Génération de l'image
[ "${HAS_TARGET:-false}" != "true" ] && OUTPUT_IMG="${DIR_TMP}/wallpaper.svg"
[ -f ${OUTPUT_IMG} ] && rm -f ${OUTPUT_IMG}

xsltproc -o "${OUTPUT_IMG}" "${TEMPLATE_FILE}" "${DATA_XML}"
[ ! -f "${OUTPUT_IMG}" ] && print_error "Problème dans la génération du papier-peint"  && exit 1
#------------------------------------------------------------------
# Installation de l'image générée
[ "${HAS_INSTALL:-false}" = "true" ] && {
WALLPAPER_DIR="${HOME}/.wallpapers"
mkdir -p "${WALLPAPER_DIR}"
WALLPAPER_FILE="$(basename ${OUTPUT_IMG})"
cp "${OUTPUT_IMG}" "${WALLPAPER_DIR}/${WALLPAPER_FILE}"
[ -f "${WALLPAPER_DIR}/${WALLPAPER_FILE}" ] && gsettings set org.gnome.desktop.background picture-uri "${WALLPAPER_DIR}/${WALLPAPER_FILE}"
}
#------------------------------------------------------------------
# Nettoyage
[ -f ${OUTPUT_IMG} ] && rm -f ${DATA_XML}
[ -f ${OUTPUT_IMG} ] && rm -f ${DATA_FILE}
#------------------------------------------------------------------
exit 0
