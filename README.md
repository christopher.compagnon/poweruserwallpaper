# PowerUserWallpaper

Génération d'un papier-peint avec des raccourcis en surimpression (pour GNU+Linux).

Il encapsule votre papier-peint préféré dans un SVG pour y ajouter les raccourcis clavier en surimpression et produit un nouveau papier-peint.


## Systèmes d'exploitation compatibles

- GNU+Linux

## Dépendances

- xsltproc

## Usage

    createImage.sh [-f nom de fichier] [-b couleur] [-d nombrexnombre] [-o décimal] [-c couleur] [-w nom de fichier] [-t modèle] [-l] [-i] [-h]

**-f (filename)** : chemin + nom vers le fichier qui servira d'image de fond.

**-b (background)** : couleur (hexadécimale) du fond si pas d'utilisation d'image. Obligatoire si pas d'image définie.

**-d (display)** : taille de l'écran affichant l'image produite. Obligatoire.

**-o (opacity)** : opacité du masque utilisé pour augmenter le constraste. Valeur décimale, comprise entre 0.00 et 1.00. 0.00 (invisible) si pas définie.

**-c (color)** : couleur (hexadécimale) du tracé de la surimpression. ffffff (blanc) si pas définie.

**-w (wallpaper)** : chemin + nom vers le fichier SVG généré. Si pas défini, même chemin que la source, avec pour nom _wallpaper.svg_ par défaut.

**-t (template)** : modèle de génération SVG.

**-l (list)** : liste des modèles disponibles.

**-i (install)** : installe l'image générée comme papier-peint (Gnome Shell).

**-h (help)** : affichage de l'aide.


## Exemples

### Papier-peint avec fond uni

L'option -b permet de définir un fond uni :


    createImage.sh -b 0461a7 -d 1980x1020 -w mon_papier-peint.svg

Pas d'image source. Génère une image de 1980x1020 pixels SVG avec pour fond une couleur unie de code #0461a7.

### Papier-peint avec image

L'option -f en remplacement de l'option -b permet de définir une image à incorporer en fond :


    createImage.sh -f mon_image_source.jpg -d 1980x1020 -w mon_papier-peint.svg

Utilisation de image mon_image_source.jpg comme arrière-plan. Génère une image SVG de 1980x1020 pixels.


    createImage.sh -f mon_image_source.jpg -d 1980x1020 -c 000000 -o 0.75 -w mon_papier-peint.svg

Utilisation de image mon_image_source.jpg comme arrière-plan. Génère une image SVG de 1980x1020 pixels avec un tracé de couleur noire (code #000000) et une opacité de 0.75.


## Ajouter des modèles

Les modèles sont définis dans le répertoire *templates* par des transformations XSL qui se chargent de générer le SVG.

Dans ce répertoire, vous trouverez :
- **templates.lst** : liste des modèles (pour l'option -l); ajouter votre modèle dedans. Le nom de modèle est le même que le fichier xsl sans son extension.
- **transform.xsl** : définition des variables et des fonctions de base (ne pas modifier).
- **new.template.lang.xsl** : modèle de fichier modèle. Utiliser ce fichier d'exemple pour créer un nouveau modèle. Votre surimpression est à définir dans la fonction *svg.defs.graphic*.

Les autres fichiers sont les modèles.

Par exemple *gnome.shell.shortcuts.fr.xsl* est le modèle qui génère les raccourcis Gnome Shell en français.

## Licence

[CC0](https://creativecommons.org/share-your-work/public-domain/cc0/ "Creative Commons, CC0")

