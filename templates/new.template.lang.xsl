<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet 
version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
xmlns:xs="http://www.w3.org/2001/XMLSchema"
>
<!-- ========================== Imports =========================== -->
<xsl:import href="transform.xsl"/>

<!-- ========================== Output =========================== -->
<xsl:output 
method="xml" 
indent="yes"
encoding="utf-8"/>

<!-- ========================== Templates =========================== -->
<xsl:template name="svg.title">
<title>Your title</title>
<desc>Description</desc>
</xsl:template>


<xsl:template name="svg.defs.graphic">
<g id="graphic">
<!-- define your graphic here -->
<g>
</xsl:template>


<xsl:template match="/">
	
<svg
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink"
version="1.1"
width="{$display.width}px"
height="{$display.height}px"
viewbox="0 0 {$display.width} {$display.height}">

<xsl:call-template name="svg.title"/>
<defs>
<xsl:call-template name="svg.defs.background"/>
<xsl:call-template name="svg.defs.mask"/>
<xsl:call-template name="svg.defs.graphic"/>
</defs>

<xsl:call-template name="svg.draw"/>

</svg>
	
</xsl:template>	
<!-- ========================== actions par défaut =========================== -->
<xsl:template match="text()"/>
</xsl:stylesheet>